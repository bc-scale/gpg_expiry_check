# gpg_expiry_check

check the expiry of GPG keys by their URLs and output status in JSON format

#### usage

    # docker-compose up

#### custom urls

    # files='["https://yum.datadoghq.com/DATADOG_MASTER_KEY_20200908.public"]' docker-compose up

#### results

    # cat logs/output.json
    {"url":"https://yum.datadoghq.com/DATADOG_RPM_KEY.public","is_valid_gpg":true,"expires_on":"-1","days_left":0,"fingerprint":"60A389A44A0C32BAE3C03F0B069B56F54172A230","is_expired":true}
    {"url":"https://yum.datadoghq.com/DATADOG_RPM_KEY_E09422B3.public","is_valid_gpg":true,"expires_on":"2022-06-28","days_left":147,"fingerprint":"A4C0B90D7443CF6E4E8AA341F1068E14E09422B3","is_expired":false}
    {"url":"https://yum.datadoghq.com/DATADOG_RPM_KEY_CURRENT.public","is_valid_gpg":true,"expires_on":"2022-06-28","days_left":147,"fingerprint":"A4C0B90D7443CF6E4E8AA341F1068E14E09422B3","is_expired":false}
    {"url":"https://yum.datadoghq.com/DATADOG_RPM_KEY.public.E09422B3","is_valid_gpg":true,"expires_on":"2022-06-28","days_left":147,"fingerprint":"A4C0B90D7443CF6E4E8AA341F1068E14E09422B3","is_expired":false}
    {"url":"https://yum.datadoghq.com/DATADOG_MASTER_KEY_20200908.public","is_valid_gpg":true,"expires_on":"2024-09-07","days_left":949,"fingerprint":"D75CEA17048B9ACBF186794B32637D44F14F620E","is_expired":false}
    {"url":"https://yum.datadoghq.com/DATADOG_RPM_KEY_20200908.public","is_valid_gpg":true,"expires_on":"2024-09-07","days_left":949,"fingerprint":"C6559B690CA882F023BDF3F63F4D1729FD4BF915","is_expired":false}
